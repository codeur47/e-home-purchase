package com.yorosoft.ehomepurchase.repository;

import com.yorosoft.ehomepurchase.model.Category;
import com.yorosoft.ehomepurchase.model.Purchase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
    Category findByTitle(String title);
}
