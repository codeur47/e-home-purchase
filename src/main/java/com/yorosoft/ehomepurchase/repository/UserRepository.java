package com.yorosoft.ehomepurchase.repository;

import com.yorosoft.ehomepurchase.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findUserByUsername(String username);
    boolean existsUserByUsername(String username);
}
