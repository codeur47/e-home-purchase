package com.yorosoft.ehomepurchase.repository;

import com.yorosoft.ehomepurchase.model.Category;
import com.yorosoft.ehomepurchase.model.Purchase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface PurchaseRepository extends JpaRepository<Purchase, Long> {
    List<Purchase> findAllByCategory(Category category);
}
