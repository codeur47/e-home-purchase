package com.yorosoft.ehomepurchase.controller;

import com.yorosoft.ehomepurchase.model.Category;
import com.yorosoft.ehomepurchase.model.Purchase;
import com.yorosoft.ehomepurchase.service.CategoryService;
import com.yorosoft.ehomepurchase.service.PurchaseService;
import com.yorosoft.ehomepurchase.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;

@Controller
public class PurchaseController {

    private final CategoryService categoryService;
    private final UserService userService;
    private final PurchaseService purchaseService;

    @Autowired
    public PurchaseController(CategoryService categoryService, UserService userService, PurchaseService purchaseService) {
        this.categoryService = categoryService;
        this.userService = userService;
        this.purchaseService = purchaseService;
    }



    @GetMapping("/purchase")
    public String category(Model model){
        model.addAttribute("purchase", new Purchase());
        model.addAttribute("purchaseList", purchaseService.findAll());
        model.addAttribute("categoryList", categoryService.findAll());
        model.addAttribute("totalPrice", purchaseService.findAll()
                                                    .stream()
                                                    .mapToDouble(pucharse -> pucharse.getTotal().doubleValue())
                                                    .sum());
        return "purchase";
    }

    @PostMapping("/purchase")
    public String create(@ModelAttribute("purchase") Purchase purchase,@ModelAttribute("purchaseDate") String purchaseDate,
                         @ModelAttribute("purchaseCategory") String purchaseCategory,
                         Model model, Principal principal) throws ParseException {
        purchase.setDate(LocalDateTime.parse(purchaseDate));
        purchase.setCategory(categoryService.findByLibelle(purchaseCategory));
        purchase.setUser(userService.findUserByUsername(principal.getName()));
        purchase.setTitle(purchase.getTitle().toUpperCase());
        purchase.setTotal(purchase.getPrice().multiply(new BigDecimal(purchase.getQuantity())));
        model.addAttribute("purchase", new Purchase());
        model.addAttribute("purchaseList", purchaseService.findAll());
        model.addAttribute("categoryList", categoryService.findAll());
        purchaseService.create(purchase);
        return "redirect:/purchase";
    }

    @GetMapping("/deletePurchase")
    public String delete(@RequestParam("id") Long id,Model model){
        Purchase purchase = purchaseService.findById(id);
        purchaseService.delete(purchase);
        model.addAttribute("purchase", new Purchase());
        model.addAttribute("purchaseList", purchaseService.findAll());
        model.addAttribute("categoryList", categoryService.findAll());
        return "redirect:/purchase";
    }
}
