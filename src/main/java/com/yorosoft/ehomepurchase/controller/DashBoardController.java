package com.yorosoft.ehomepurchase.controller;

import com.yorosoft.ehomepurchase.model.Category;
import com.yorosoft.ehomepurchase.service.CategoryService;
import com.yorosoft.ehomepurchase.service.PurchaseService;
import com.yorosoft.ehomepurchase.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Controller
public class DashBoardController {

    private final CategoryService categoryService;
    private final UserService userService;
    private final PurchaseService purchaseService;

    @Autowired
    public DashBoardController(CategoryService categoryService, UserService userService, PurchaseService purchaseService) {
        this.categoryService = categoryService;
        this.userService = userService;
        this.purchaseService = purchaseService;
    }



    @GetMapping("/dashboard")
    public String dashboard(Principal principal,Model model){
        model.addAttribute("user", userService.findUserByUsername(principal.getName()));
        model.addAttribute("categorySize",categoryService.findAll().size());
        model.addAttribute("purchaseSize",purchaseService.findAll().size());

        model.addAttribute("tenLastPurchaseByDate",purchaseService.findAll()
                .stream()
                .sorted((pur1,pur2) -> pur2.getDate().compareTo(pur1.getDate()))
                .limit(10)
                .collect(Collectors.toList()));

        model.addAttribute("topTenPurchaseByTotal",purchaseService.findAll()
                .stream()
                .sorted((pur1,pur2)-> pur2.getTotal().compareTo(pur1.getTotal()))
                .limit(10)
                .collect(Collectors.toList()));

        return "dashboard";

    }
}
