package com.yorosoft.ehomepurchase.controller;

import com.yorosoft.ehomepurchase.model.Category;
import com.yorosoft.ehomepurchase.model.User;
import com.yorosoft.ehomepurchase.service.CategoryService;
import com.yorosoft.ehomepurchase.service.PurchaseService;
import com.yorosoft.ehomepurchase.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Controller
public class CategoryController {

    private final CategoryService categoryService;
    private final UserService userService;
    private final PurchaseService purchaseService;

    @Autowired
    public CategoryController(CategoryService categoryService, UserService userService, PurchaseService purchaseService) {
        this.categoryService = categoryService;
        this.userService = userService;
        this.purchaseService = purchaseService;
    }



    @GetMapping("/category")
    public String category(Model model){
        model.addAttribute("category", new Category());
        List<Category> categoryList = categoryService.findAll();
        model.addAttribute("categoryList",categoryList);
        model.addAttribute("size",categoryService.findAll().size());
        return "category";
    }

    @PostMapping("/category")
    public String create(@ModelAttribute("category") Category category, Model model, Principal principal){
        category.setUser(userService.findUserByUsername(principal.getName()));
        if (categoryService.findByLibelle(category.getTitle())==null){
            categoryService.create(category);
            model.addAttribute("category", new Category());
        }
        else
            model.addAttribute("categoryExist", true);

        model.addAttribute("size",categoryService.findAll().size());
        model.addAttribute("categoryList",categoryService.findAll());
        return "category";
    }

    @GetMapping("/delete")
    public String delete(@RequestParam("id") Long id,Model model){
        Category category = categoryService.findById(id);
        if (purchaseService.findAllByCategory(category).size()>0)
            model.addAttribute("used",true);
        else
            categoryService.delete(category);

        model.addAttribute("category", new Category());
        model.addAttribute("size",categoryService.findAll().size());
        model.addAttribute("categoryList",categoryService.findAll());
        return "category";
    }

    @GetMapping("/edit")
    public String edit(@RequestParam("id") Long id,Model model){
        model.addAttribute("category", categoryService.findById(id));
        model.addAttribute("size",categoryService.findAll().size());
        model.addAttribute("categoryList",categoryService.findAll());
        return "category";
    }
}
