package com.yorosoft.ehomepurchase.controller;

import com.yorosoft.ehomepurchase.model.Role;
import com.yorosoft.ehomepurchase.model.User;
import com.yorosoft.ehomepurchase.model.UserRole;
import com.yorosoft.ehomepurchase.service.RoleService;
import com.yorosoft.ehomepurchase.service.UserService;
import com.yorosoft.ehomepurchase.utility.Constant;
import it.sauronsoftware.jave.AudioAttributes;
import it.sauronsoftware.jave.Encoder;
import it.sauronsoftware.jave.EncodingAttributes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.io.File;
import java.security.Principal;
import java.util.HashSet;
import java.util.Set;

@Controller
public class UserController {

    private final UserService userService;
    private final RoleService roleService;

    @Autowired
    public UserController(UserService userService, RoleService roleService) {
        this.userService = userService;
        this.roleService = roleService;
    }

    @GetMapping({"/", "/index","/login"})
    public String home(Model model) {
        model.addAttribute("user", new User());
        return "login";
    }

    @PostMapping("/signup")
    public String createUser(@ModelAttribute("user") User user, Model model){
        if (userService.checkUserExistByUsername(user.getUsername())) {
            return "signup";
        }else {
            Set<UserRole> userRoles = new HashSet<>();
            Role role = roleService.findByName(Constant.ROLE_USER);
            userRoles.add(new UserRole(user, role));
            userService.createUser(user,userRoles);
            model.addAttribute("user", new User());
            return "login";
        }
    }

}
