package com.yorosoft.ehomepurchase.model;

import com.yorosoft.ehomepurchase.utility.BaseEntity;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Purchase extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String title;

    private String description;

    private BigDecimal price;

    private Integer quantity;

    private BigDecimal total;

    private LocalDateTime date;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id")
    private Category category;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    private Date createdAt;

    private Date updaDate;

    @PrePersist
    public void onCreate() {this.createdAt = new Date();}

    @PreUpdate
    public void onUpdate() {this.updaDate = new Date(); }
}
