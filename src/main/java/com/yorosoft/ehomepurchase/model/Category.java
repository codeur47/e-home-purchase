package com.yorosoft.ehomepurchase.model;

import com.yorosoft.ehomepurchase.utility.BaseEntity;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String title;

    @OneToMany(mappedBy = "category", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Purchase> purchaseList;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    public Date createdAt;

    public Date updaDate;

    @PrePersist
    public void onCreate() {this.createdAt = new Date();}

    @PreUpdate
    public void onUpdate() {this.updaDate = new Date(); }

}
