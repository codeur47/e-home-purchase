package com.yorosoft.ehomepurchase.service;

import com.yorosoft.ehomepurchase.model.Role;
import com.yorosoft.ehomepurchase.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {

    private final RoleRepository roleRepository;

    @Autowired
    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public Role findByName(String s) {
        return roleRepository.findByName(s);
    }
}
