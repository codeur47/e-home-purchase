package com.yorosoft.ehomepurchase.service;

import com.yorosoft.ehomepurchase.model.Category;
import com.yorosoft.ehomepurchase.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryService {

    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public Category create(Category category) {
        return categoryRepository.save(category);
    }

    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    public Category findByLibelle(String s) {
        return categoryRepository.findByTitle(s);
    }

    public Category findById(Long id) {
        Optional<Category> category = categoryRepository.findById(id);
        return category.orElse(null);
    }

    public void delete(Category category) {
        categoryRepository.delete(category);
    }
}
