package com.yorosoft.ehomepurchase.service;

import com.yorosoft.ehomepurchase.model.User;
import com.yorosoft.ehomepurchase.model.UserRole;
import com.yorosoft.ehomepurchase.repository.RoleRepository;
import com.yorosoft.ehomepurchase.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Set;

@Service
public class UserService {

    private static final Logger LOG = LoggerFactory.getLogger(UserService.class);
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, RoleRepository roleRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public User findUserByUsername(String s) { return userRepository.findUserByUsername(s);}

    public boolean checkUserExistByUsername(String s) { return userRepository.existsUserByUsername(s); }

    @Transactional
    public User createUser(User user, Set<UserRole> userRoles) {
        if (checkUserExistByUsername(user.getUsername())) LOG.info("Utilisateur avec le nom utilisateur {} existe déjà. ", user.getUsername());
        else {
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            userRoles.forEach(ur -> {
                roleRepository.save(ur.getRole());
            });
            user.getUserRoles().addAll(userRoles);
        }
        return userRepository.save(user);
    }
}
