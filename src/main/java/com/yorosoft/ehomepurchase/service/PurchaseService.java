package com.yorosoft.ehomepurchase.service;

import com.yorosoft.ehomepurchase.model.Category;
import com.yorosoft.ehomepurchase.model.Purchase;
import com.yorosoft.ehomepurchase.repository.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PurchaseService {

    private final PurchaseRepository purchaseRepository;

    @Autowired
    public PurchaseService(PurchaseRepository purchaseRepository) {
        this.purchaseRepository = purchaseRepository;
    }

    public List<Purchase> findAllByCategory(Category category){
        return purchaseRepository.findAllByCategory(category);
    }

    public List<Purchase> findAll(){
        return purchaseRepository.findAll();
    }

    public Purchase create(Purchase purchase) { return purchaseRepository.save(purchase); }

    public Purchase findById(Long id) {
        Optional<Purchase> purchase = purchaseRepository.findById(id);
        return purchase.orElse(null);
    }

    public void delete(Purchase purchase) {
        purchaseRepository.delete(purchase);
    }
}
