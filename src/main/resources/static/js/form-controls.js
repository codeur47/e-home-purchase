// Class definition
var KTFormControls = function () {
	// Private functions
	var _initDemo1 = function () {
		FormValidation.formValidation(
			document.getElementById('kt_form_1'),
			{
				fields: {
					title: {
						validators: {
							notEmpty: {
								message: 'Le libelle est obligatoire'
							}
						}
					},
					description: {
						validators: {
							notEmpty: {
								message: 'La description est obligatoire'
							}
						}
					},
					price: {
						validators: {
							notEmpty: {
								message: 'Le prix est obligatoire'
							}
						}
					},
					quantity: {
						validators: {
							notEmpty: {
								message: 'La quantite est obligatoire'
							}
						}
					},
					date: {
						validators: {
							notEmpty: {
								message: 'La date est obligatoire'
							}
						}
					},
					purchaseCategory: {
						validators: {
							notEmpty: {
								message: 'La categorie est obligatoire'
							}
						}
					},
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					// Bootstrap Framework Integration
					bootstrap: new FormValidation.plugins.Bootstrap(),
					// Validate fields when clicking the Submit button
					submitButton: new FormValidation.plugins.SubmitButton(),
            		// Submit the form when all fields are valid
            		defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
				}
			}
		);
	}

	return {
		// public functions
		init: function() {
			_initDemo1();
		}
	};
}();

jQuery(document).ready(function() {
	KTFormControls.init();
});
