"use strict";

// Class Definition
var KTLogin = function() {
    var _login;

    var _showForm = function(form) {
        var cls = 'login-' + form + '-on';
        var form = 'kt_login_' + form + '_form';

        _login.removeClass('login-signin-on');
        _login.removeClass('login-signup-on');

        _login.addClass(cls);

        KTUtil.animateClass(KTUtil.getById(form), 'animate__animated animate__backInUp');
    }

    var _handleSignInForm = function() {
        var validation;

        // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
        validation = FormValidation.formValidation(
			KTUtil.getById('kt_login_signin_form'),
			{
				fields: {
					username: {
						validators: {
							notEmpty: {
								message: 'Le nom utilisateur est obligatoire'
							}
						}
					},
					password: {
						validators: {
							notEmpty: {
								message: 'Le mot de passe est obligatoire'
							}
						}
					}
				},
				plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap()
				}
			}
		);

        $('#kt_login_signin_submit').on('click', function (e) {

            validation.validate().then(function(status) {
		        if (status !== 'Valid') {
					e.preventDefault();
                    swal.fire({
						text: "Désolé, il semble que des erreurs ont été détectées, veuillez réessayer.",
						icon: "error",
						buttonsStyling: false,
						confirmButtonText: "Ok, compris !",
						customClass: {
							confirmButton: "btn font-weight-bold btn-light-primary"
						}
		            }).then(function() {
						KTUtil.scrollTop();
					});
				}
		    });
        });

        // Handle signup
        $('#kt_login_signup').on('click', function (e) {
            e.preventDefault();
            _showForm('signup');
        });
    }

    var _handleSignUpForm = function(e) {
        var validation;
        var form = KTUtil.getById('kt_login_signup_form');

        // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
        validation = FormValidation.formValidation(
			form,
			{
				fields: {
					lastname: {
						validators: {
							notEmpty: {
								message: 'Le nom est obligatoire'
							}
						}
					},
					firstname: {
						validators: {
							notEmpty: {
								message: 'Le prenom est obligatoire'
							}
						}
					},
					username: {
                        validators: {
							notEmpty: {
								message: 'Le nom utilisateur est obligatoire'
							}
						}
					},
                    password: {
                        validators: {
                            notEmpty: {
                                message: 'Le nom de passe est obligatoire'
                            }
                        }
                    },
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap()
				}
			}
		);

        $('#kt_login_signup_submit').on('click', function (e) {
            validation.validate().then(function(status) {
		        if (status !== 'Valid') {
					e.preventDefault();
                    swal.fire({
						text: "Désolé, il semble que des erreurs ont été détectées, veuillez réessayer.",
						icon: "error",
						buttonsStyling: false,
						confirmButtonText: "Ok, compris !",
						customClass: {
							confirmButton: "btn font-weight-bold btn-light-primary"
						}
		            }).then(function() {
						KTUtil.scrollTop();
					});
				}
		    });
        });

        // Handle cancel button
        $('#kt_login_signup_cancel').on('click', function (e) {
            e.preventDefault();

            _showForm('signin');
        });
    }

	var _handleCategoryForm = function(e) {
		var validation;
		var form = KTUtil.getById('kt_category_form');

		// Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
		validation = FormValidation.formValidation(
			form,
			{
				fields: {
					lastname: {
						validators: {
							notEmpty: {
								title: 'Le libelle est obligatoire'
							}
						}
					},
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap()
				}
			}
		);

		$('#kt_category_register_submit').on('click', function (e) {
			validation.validate().then(function(status) {
				if (status !== 'Valid') {
					e.preventDefault();
					swal.fire({
						text: "Désolé, il semble que des erreurs ont été détectées, veuillez réessayer.",
						icon: "error",
						buttonsStyling: false,
						confirmButtonText: "Ok, compris !",
						customClass: {
							confirmButton: "btn font-weight-bold btn-light-primary"
						}
					}).then(function() {
						KTUtil.scrollTop();
					});
				}
			});
		});
		// Handle cancel button
		$('#kt_category_register_cancel').on('click', function (e) {
			e.preventDefault();

		});
	}

    // Public Functions
    return {
        // public functions
        init: function() {
            _login = $('#kt_login');

            _handleSignInForm();
            _handleSignUpForm();
            _handleCategoryForm();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTLogin.init();
});
