"use strict";
// Class Initialization
jQuery(document).ready(function() {
	let price = document.querySelector('#price');
	let quantity = document.querySelector('#quantity');
	let total = document.querySelector('#total');

	quantity.value = 1;
	price.value = 1;
	total.value = 1;

	price.addEventListener("change", function () {
		total.value =  Number(this.value)*Number(quantity.value);
	});

	quantity.addEventListener("change", function () {
		total.value =  Number(this.value)*Number(price.value);
	});

});
